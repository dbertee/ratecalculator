package com.zopa.ratecalculator;

import com.opencsv.bean.CsvBindByName;

/**
 * The representation of a lender loaded from a CSV.
 */
public class CSVLender implements Comparable<CSVLender> {

    @CsvBindByName
    private float rate;

    @CsvBindByName
    private int available;

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    @Override
    public int compareTo(CSVLender o) {
        if (this.getRate() == o.getRate())
            return 0;
        return this.getRate() < o.getRate() ? -1 : 1;
    }
}
