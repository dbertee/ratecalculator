package com.zopa.ratecalculator;

import java.util.List;

/**
 * The root of the application. This is set to run in maven assembly plugin as well.
 */
public class RateCalculatorApplication {

    private static final String POUND_SIGN = "£";
    private static final int NUMBER_OF_ARGUMENTS = 2;

    public static void main(String[] args) {
        checkNumberOfArguments(args);

        int loan_amount = getLoanAmount(args[1]);

        List<CSVLender> lenders = getListOfLenders(args[0]);

        ProvidedQuote providedQuote = getProvidedQuote(loan_amount, lenders);

        printProvidedQuote(providedQuote);
    }

    private static ProvidedQuote getProvidedQuote(int loan_amount, List<CSVLender> lenders) {
        BestRateCalculator bestRateCalculator = new BestRateCalculatorZopa();
        float rate = bestRateCalculator.calculateBestRate(lenders, loan_amount);
        AnnuityCalculator calculator = new AnnuityCalculatorThreeYears();
        return calculator.calculateAnnuity(loan_amount, rate);
    }

    private static List<CSVLender> getListOfLenders(String filename) {
        LendersCSVReader csvParser = new LendersCSVReader();
        return csvParser.getLenders(filename);
    }

    private static void checkNumberOfArguments(String[] args) {
        if (args.length != NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException("Parameters should follow the [market_file] [loan_amount] order.");
        }
    }

    private static int getLoanAmount(String loan) {
        return new LoanInputValidator().validateInput(loan);
    }

    private static void printProvidedQuote(ProvidedQuote providedQuote) {
        System.out.println("Requested amount: " + POUND_SIGN + providedQuote.getRequestedAmount());
        System.out.println("Rate: " + providedQuote.getRate());
        System.out.println("Monthly Repayment: " + POUND_SIGN + providedQuote.getMonthlyRepayment());
        System.out.println("Total Repayment: " + POUND_SIGN + providedQuote.getTotalRepayment());
    }
}
