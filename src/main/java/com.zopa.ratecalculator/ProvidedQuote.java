package com.zopa.ratecalculator;

/**
 * Representation of the final result (provided quote) with a builder.
 */
public class ProvidedQuote {

    private int requestedAmount;
    private String rate;
    private float monthlyRepayment;
    private float totalRepayment;

    public int getRequestedAmount() {
        return requestedAmount;
    }

    public String getRate() {
        return rate;
    }

    public float getMonthlyRepayment() {
        return monthlyRepayment;
    }

    public float getTotalRepayment() {
        return totalRepayment;
    }

    public ProvidedQuote(Builder builder) {
        this.requestedAmount = builder.requestedAmount;
        this.rate = builder.rate;
        this.monthlyRepayment = builder.monthlyRepayment;
        this.totalRepayment = builder.totalRepayment;
    }

    public static class Builder {
        private int requestedAmount;
        private String rate;
        private float monthlyRepayment;
        private float totalRepayment;

        public Builder withRequestedAmount(int requestedAmount) {
            this.requestedAmount = requestedAmount;
            return this;
        }

        public Builder withRate(String rate) {
            this.rate = rate;
            return this;
        }

        public Builder withMonthlyRepayment(float monthlyRepayment) {
            this.monthlyRepayment = monthlyRepayment;
            return this;
        }

        public Builder withTotalRepayment(float totalRepayment) {
            this.totalRepayment = totalRepayment;
            return this;
        }

        public ProvidedQuote build() {
            return new ProvidedQuote(this);
        }

    }
}
