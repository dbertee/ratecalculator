package com.zopa.ratecalculator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Best rate calculator for Zopa.
 * Takes the lowest possible rates from the lenders and calculates the average of them.
 */
public class BestRateCalculatorZopa implements BestRateCalculator {

    private static final int THREE_DECIMALS = 3;

    @Override
    public float calculateBestRate(final List<CSVLender> lenders, final int loan) {
        checkSufficientOffers(lenders, loan);
        Map<Float, Integer> distinctRatesWithAmounts = mergeSameRates(lenders);
        List<Float> ratesToCalculate = getRatesToCalculateAverage(loan, distinctRatesWithAmounts);
        return round(calculateAverageRate(ratesToCalculate), THREE_DECIMALS);
    }

    private void checkSufficientOffers(List<CSVLender> lenders, int loan) {
        if (lenders == null || lenders.isEmpty() || lenders.stream().mapToInt(CSVLender::getAvailable).sum() < loan) {
            throw new InsufficientOffersException();
        }
    }

    private Map<Float, Integer> mergeSameRates(List<CSVLender> lenders) {
        Collections.sort(lenders);
        Map<Float, Integer> ratesWithAmounts = new LinkedHashMap<>();
        for (CSVLender csvLender : lenders) {
            if (ratesWithAmounts.containsKey(csvLender.getRate())) {
                ratesWithAmounts.put(csvLender.getRate(), csvLender.getAvailable() + ratesWithAmounts.get(csvLender.getRate()));
            } else {
                ratesWithAmounts.put(csvLender.getRate(), csvLender.getAvailable());
            }
        }
        return ratesWithAmounts;
    }

    private List<Float> getRatesToCalculateAverage(int loan, Map<Float, Integer> distinctRatesWithAmounts) {
        int remainingLoan = loan;
        List<Float> ratesToCalculate = new ArrayList<>();
        Iterator<Map.Entry<Float, Integer>> iterator = distinctRatesWithAmounts.entrySet().iterator();
        while (iterator.hasNext() && remainingLoan > 0) {
            Map.Entry<Float, Integer> entry = iterator.next();
            remainingLoan -= entry.getValue();
            ratesToCalculate.add(entry.getKey());
        }
        return ratesToCalculate;
    }

    private float calculateAverageRate(List<Float> rates) {
        float sumOfRates = 0;
        for (float rate : rates) {
            sumOfRates += rate;
        }
        return sumOfRates / rates.size();
    }

    private float round(float number, int decimalPlace) {
        BigDecimal bd = new BigDecimal(number);
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
}
