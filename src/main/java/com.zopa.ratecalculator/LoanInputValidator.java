package com.zopa.ratecalculator;

import org.apache.commons.lang3.StringUtils;

/**
 * Validator for the given loan.
 */
public class LoanInputValidator {

    /** Validates loan whether it's a number or eligible for the documented values.
     * @param loan The requested amount.
     * @return The value read from the input.
     * @throws NumberFormatException when value is invalid.
     */
    public int validateInput(String loan) throws NumberFormatException {
        if (!StringUtils.isNumeric(loan)) {
            throw new NumberFormatException("Loan must be a number!");
        } else if (Integer.parseInt(loan) < 1000 || Integer.parseInt(loan) > 15000 || (Integer.parseInt(loan) % 100) != 0) {
            throw new NumberFormatException("Loan must be between £1000 and £15000 of any £100 increment inclusive!");
        }

        return Integer.parseInt(loan);
    }
}
