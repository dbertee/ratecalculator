package com.zopa.ratecalculator;

/**
 * Thrown to indicate the market does not have sufficient offers from lenders.
 */
public class InsufficientOffersException extends RuntimeException {

    public static String INSUFFICIENT_OFFERS = "Market has insufficient offers from lenders to satisfy the loan.";

    public InsufficientOffersException() {
        super(INSUFFICIENT_OFFERS);
    }
}
