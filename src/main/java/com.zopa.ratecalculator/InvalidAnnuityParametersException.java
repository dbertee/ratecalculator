package com.zopa.ratecalculator;

/**
 * Thrown when parameters are invalid for calculating annuity.
 */
public class InvalidAnnuityParametersException extends RuntimeException {

    private static final String UNABLE_TO_CALCULATE_ANNUITY = "Unable to calculate annuity - invalid parameters!";

    public InvalidAnnuityParametersException() {
        super(UNABLE_TO_CALCULATE_ANNUITY);
    }
}
