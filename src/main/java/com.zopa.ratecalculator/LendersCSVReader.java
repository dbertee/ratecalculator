package com.zopa.ratecalculator;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Reader and parser for a list of {@link CSVLender}s.
 */
public class LendersCSVReader {

    /**
     * Tries to read a file with the given name from classpath and create a list of {@link CSVLender}s if possible.
     * @param filename The CSV file containing the lenders.
     * @return A list of {@link CSVLender}s.
     * @throws ParsingCSVException when file cannot be found or CSV is in incorrect format.
     */
    public List<CSVLender> getLenders(final String filename) throws ParsingCSVException{
        List<CSVLender> csvLenders = new ArrayList<>();
        try (InputStream in = this.getClass().getClassLoader().getResourceAsStream(filename);
             Reader reader = new InputStreamReader(in)) {
            CsvToBean<CSVLender> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(CSVLender.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();
            csvToBean.forEach(csvLenders::add);
        } catch (Exception e) {
            throw new ParsingCSVException(e, filename);
        }
        return csvLenders;
    }
}
