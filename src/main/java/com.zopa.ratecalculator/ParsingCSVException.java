package com.zopa.ratecalculator;

/**
 * Thrown to indicate missing or invalid CSV file.
 */
public class ParsingCSVException extends RuntimeException {

    private static final String FAILED_TO_PARSE_CSV = "Failed to parse CSV! File name: ";

    public ParsingCSVException(Throwable cause, String filename) {
        super(FAILED_TO_PARSE_CSV + filename, cause);
    }
}
