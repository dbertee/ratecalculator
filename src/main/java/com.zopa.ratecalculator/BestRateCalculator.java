package com.zopa.ratecalculator;

import java.util.List;

/**
 * Interface for calculating the best possible rate from a pool of lenders.
 */
public interface BestRateCalculator {

    /**
     * Calculates the best possible rate from a pool of lenders taking loan into account.
     * @param lenders The pool of {@link CSVLender}s.
     * @param loan The amount of loan a person wants to borrow.
     * @return The calculated rate
     */
    float calculateBestRate(List<CSVLender> lenders, int loan);
}
