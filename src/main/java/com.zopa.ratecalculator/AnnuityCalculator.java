package com.zopa.ratecalculator;

/**
 * Interface for calculating annuity.
 */
public interface AnnuityCalculator {

    /**
     * Calculates annuity for the given loan and rate.
     *
     * @param loan The amount of loan a person wants to borrow.
     * @param rate The rate used for the calculation.
     * @return ProvidedQuote with all the necessary results.
     */
    ProvidedQuote calculateAnnuity(int loan, float rate);
}
