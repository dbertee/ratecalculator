package com.zopa.ratecalculator;

import java.math.BigDecimal;

/**
 * Calculates annuity for three years of loan.
 */
public class AnnuityCalculatorThreeYears implements AnnuityCalculator {

    private static final int ONE_YEAR = 12;
    private static final int THREE_YEARS = 36;
    private static final int TWO_DECIMALS = 2;
    private static final int RATE_MULTIPLIER = 100;

    @Override
    public ProvidedQuote calculateAnnuity(final int loan, final float rate) {
        validateParameters(loan, rate);
        float monthlyRate = rate / ONE_YEAR + 1;
        float monthlyRepayment = loan * ((float) Math.pow(monthlyRate, THREE_YEARS)) / ((float) (Math.pow(monthlyRate, THREE_YEARS) - 1) / (monthlyRate - 1));
        return new ProvidedQuote.Builder()
                .withRequestedAmount(loan)
                .withRate(Float.toString(rate* RATE_MULTIPLIER) + "%")
                .withMonthlyRepayment(round(monthlyRepayment, TWO_DECIMALS))
                .withTotalRepayment(round(monthlyRepayment * THREE_YEARS, TWO_DECIMALS))
                .build();
    }

    private void validateParameters(int loan, float rate) {
        if (loan <= 0 || rate > 1 || rate <= 0) {
            throw new InvalidAnnuityParametersException();
        }
    }

    private float round(float number, int decimalPlace) {
        BigDecimal bd = new BigDecimal(number);
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
}
