package com.zopa.ratecalculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AnnuityCalculatorThreeYearsTest {

    private static final int VALID_LOAN = 1000;
    private static final int INVALID_LOAN = -1000;
    private static final float VALID_RATE = 0.07f;
    private static final float INVALID_RATE = 1.2f;

    private static final String RATE_WITH_PERCENT = "7.0%";

    private AnnuityCalculatorThreeYears underTest;

    @BeforeEach
    public void test() {
        underTest = new AnnuityCalculatorThreeYears();
    }

    @Test
    public void calculateAnnuityShouldThrowInvalidAnnuityParametersExceptionWhenRateIsInvalid(){
        //GIVEN
        int loan = VALID_LOAN;
        float rate = INVALID_RATE;
        //WHEN
        Throwable exception = assertThrows(InvalidAnnuityParametersException.class, () -> {
            underTest.calculateAnnuity(loan, rate);
        });
        //THEN
        assertEquals("Unable to calculate annuity - invalid parameters!", exception.getMessage());
    }

    @Test
    public void calculateAnnuityShouldThrowInvalidAnnuityParametersExceptionWhenLoanIsInvalid(){
        //GIVEN
        int loan = INVALID_LOAN;
        float rate = VALID_RATE;
        //WHEN
        Throwable exception = assertThrows(InvalidAnnuityParametersException.class, () -> {
            underTest.calculateAnnuity(loan, rate);
        });
        //THEN
        assertEquals("Unable to calculate annuity - invalid parameters!", exception.getMessage());
    }

    @Test
    public void calculateAnnuityShouldReturnCorrectValues() {
        //GIVEN
        int loan = VALID_LOAN;
        float rate = VALID_RATE;
        //WHEN
        ProvidedQuote actual = underTest.calculateAnnuity(loan, rate);
        //THEN
        assertEquals(VALID_LOAN, actual.getRequestedAmount());
        assertEquals(RATE_WITH_PERCENT, actual.getRate());
        assertEquals(30.88f, actual.getMonthlyRepayment());
        assertEquals(1111.58f, actual.getTotalRepayment());
    }
}