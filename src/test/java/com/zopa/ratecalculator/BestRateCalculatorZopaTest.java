package com.zopa.ratecalculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BestRateCalculatorZopaTest {

    private static final int LOAN = 100;
    private static final float RATE_FIVE_PERCENT = 0.05f;
    private static final float RATE_SEVEN_PERCENT = 0.07f;
    private static final float RATE_NINE_PERCENT = 0.09f;
    private static final int AVAILABLE_FIFTY = 50;
    private static final int AVAILABLE_FIFTY_ONE = 51;
    private static final int AVAILABLE_TWENTY_FIVE = 25;
    private static final int AVAILABLE_FORTY_NINE = 49;

    private BestRateCalculatorZopa underTest;

    @BeforeEach
    public void run() {
        underTest = new BestRateCalculatorZopa();
    }

    @Test
    public void calculateBestRateShouldThrowInsufficientOffersExceptionWhenLoanIsHigherThanSumOfOffers() {
        //GIVEN
        List<CSVLender> lenders = getListOfLendersWithInsufficientOffer();
        //WHEN
        Throwable exception = assertThrows(InsufficientOffersException.class, () -> {
            underTest.calculateBestRate(lenders, LOAN);
        });
        //THEN
        assertEquals("Market has insufficient offers from lenders to satisfy the loan.", exception.getMessage());
    }

    @Test
    public void calculateBestRateShouldThrowInsufficientOffersExceptionWhenThereAreNoLenders(){
        //GIVEN
        List<CSVLender> noLenders = new ArrayList<>();
        //WHEN
        Throwable exception = assertThrows(InsufficientOffersException.class, () -> {
            underTest.calculateBestRate(noLenders, LOAN);
        });
        //THEN
        assertEquals("Market has insufficient offers from lenders to satisfy the loan.", exception.getMessage());
    }

    @Test
    public void calculateBestRateShouldReturnAverageOfTwoLowestRatesWhenLoanIsLowerThanTheSumOfTwoBestLenders() {
        //GIVEN
        List<CSVLender> lenders = getListOfLendersWithSufficientOffer();
        //WHEN
        float actual = underTest.calculateBestRate(lenders, LOAN);
        //THEN
        assertEquals(0.06f, actual);
    }

    @Test
    public void calculateBestRateShouldReturnAverageOfThreeLowestRatesWhenTwoRatesAreTheSame() {
        //GIVEN
        List<CSVLender> lenders = getListOfLendersWithTwoEqualRates();
        //WHEN
        float actual = underTest.calculateBestRate(lenders, LOAN);
        //THEN
        assertEquals(0.06f, actual);
    }

    private List<CSVLender> getListOfLendersWithInsufficientOffer() {
        List<CSVLender> lenders = new ArrayList<>();
        CSVLender lender = new CSVLender();
        lender.setAvailable(AVAILABLE_FIFTY);
        lender.setRate(RATE_SEVEN_PERCENT);
        lenders.add(lender);

        lender = new CSVLender();
        lender.setAvailable(AVAILABLE_FORTY_NINE);
        lender.setRate(RATE_SEVEN_PERCENT);
        lenders.add(lender);

        return lenders;
    }

    private List<CSVLender> getListOfLendersWithSufficientOffer() {
        List<CSVLender> lenders = new ArrayList<>();
        CSVLender lender = new CSVLender();
        lender.setAvailable(AVAILABLE_FIFTY);
        lender.setRate(RATE_SEVEN_PERCENT);
        lenders.add(lender);

        lender = new CSVLender();
        lender.setAvailable(AVAILABLE_FIFTY_ONE);
        lender.setRate(RATE_FIVE_PERCENT);
        lenders.add(lender);

        lender = new CSVLender();
        lender.setAvailable(AVAILABLE_FIFTY);
        lender.setRate(RATE_NINE_PERCENT);
        lenders.add(lender);

        return lenders;
    }

    private List<CSVLender> getListOfLendersWithTwoEqualRates(){
        List<CSVLender> lenders = new ArrayList<>();
        CSVLender lender = new CSVLender();
        lender.setAvailable(AVAILABLE_TWENTY_FIVE);
        lender.setRate(RATE_SEVEN_PERCENT);
        lenders.add(lender);

        lender = new CSVLender();
        lender.setAvailable(AVAILABLE_TWENTY_FIVE);
        lender.setRate(RATE_FIVE_PERCENT);
        lenders.add(lender);

        lender = new CSVLender();
        lender.setAvailable(AVAILABLE_FIFTY);
        lender.setRate(RATE_FIVE_PERCENT);
        lenders.add(lender);

        lender = new CSVLender();
        lender.setAvailable(AVAILABLE_FIFTY);
        lender.setRate(RATE_NINE_PERCENT);
        lenders.add(lender);

        return lenders;
    }
}