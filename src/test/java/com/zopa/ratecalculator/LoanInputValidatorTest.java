package com.zopa.ratecalculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LoanInputValidatorTest {

    private static final String INVALID_LOAN_NOT_A_NUMBER = "not a number";
    private static final String INVALID_LOAN_WRONG_NUMBER = "900";
    private static final String INVALID_LOAN_NOT_DIVISIBLE_BY_ONE_HUNDRED = "1110";
    private LoanInputValidator underTest;

    @BeforeEach
    public void test() {
        underTest = new LoanInputValidator();
    }

    @Test
    public void validateInputShouldThrowNumberFormatExceptionWhenLoanIsNull() {
        //GIVEN
        String loan = null;
        //WHEN
        Throwable exception = assertThrows(NumberFormatException.class, () -> {
            underTest.validateInput(loan);
        });
        //THEN
        assertEquals("Loan must be a number!", exception.getMessage());
    }

    @Test
    public void validateInputShouldThrowNumberFormatExceptionWhenLoanIsNotANumber() {
        //GIVEN
        String loan = INVALID_LOAN_NOT_A_NUMBER;
        //WHEN
        Throwable exception = assertThrows(NumberFormatException.class, () -> {
            underTest.validateInput(loan);
        });
        //THEN
        assertEquals("Loan must be a number!", exception.getMessage());
    }

    @Test
    public void validateInputShouldThrowNumberFormatExceptionWhenLoanIsOutOfRange(){
        //GIVEN
        String loan = INVALID_LOAN_WRONG_NUMBER;
        //WHEN
        Throwable exception = assertThrows(NumberFormatException.class, () -> {
            underTest.validateInput(loan);
        });
        //THEN
        assertEquals("Loan must be between £1000 and £15000 of any £100 increment inclusive!", exception.getMessage());
    }

    @Test
    public void validateInputShouldThrowNumberFormatExceptionWhenLoanIsNotDivisibleByOneHundred(){
        //GIVEN
        String loan = INVALID_LOAN_NOT_DIVISIBLE_BY_ONE_HUNDRED;
        //WHEN
        Throwable exception = assertThrows(NumberFormatException.class, () -> {
            underTest.validateInput(loan);
        });
        //THEN
        assertEquals("Loan must be between £1000 and £15000 of any £100 increment inclusive!", exception.getMessage());
    }
}